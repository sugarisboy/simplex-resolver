﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Simplex
{
    class Utils
    {

        public static Int32 readInt32(String text, Label label)
        {
            try
            {
                return Int32.Parse(text);
            }
            catch (Exception ex)
            {
                label.Content = "Неправильный формат числа";
                return 0;
            }
        }

        public static Fraction[][] clone(Fraction[][] matrix) {
            Fraction[][] clone = new Fraction[matrix.Length][];
            for (int r = 0; r < matrix.Length; r++) {
                Fraction[] cloneRow = new Fraction[matrix[r].Length];
                for (int c = 0; c < matrix[r].Length; c++) {
                    cloneRow[c] = matrix[r][c].clone();
                }
                clone[r] = cloneRow;
            }
            return clone;
        }

        public static Fraction[][] cloneWithSwapRow(Fraction[][] matrix, int i, int j) {
            Fraction[][] clone = Utils.clone(matrix);

            for (int c = 0; c < matrix[0].Length; c++) {
                Fraction temp = clone[i][c];
                clone[i][c] = clone[j][c];
                clone[j][c] = temp;
            }
            
            return clone;
        }

        public static string castMatrixToString(Fraction[][] matrix) {
            const string template = "{0,8}";
            string result = "";
            for (int r = 0; r < matrix.Length; r++) {
                for (int c = 0; c < matrix[r].Length; c++) {
                    result += string.Format(template, matrix[r][c].ToString());
                }
                result += "\n";
            }
            return result;
        }
    }
}

﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Simplex {
    public class UiUtils {
        public static Fraction[][] readMatrix(StackPanel panel, int rowCount, int columnCount) {
            Fraction[][] matrix = new Fraction[rowCount][];

            if (rowCount == null) {
            }

            int rowCounter = 0;
            foreach (UIElement panelChild in panel.Children) {
                if (panelChild is StackPanel) {
                    int offset = 0;
                    Fraction[] rowMatrix = readRow((StackPanel) panelChild, columnCount);
                    matrix[rowCounter++] = rowMatrix;
                } else {
                    throw new Exception("Неопознаная структура RightBar");
                }
            }

            return matrix;
        }
        
        public static Fraction[] readRow(StackPanel panel, int length) {
            int offset = 0;
            Fraction[] rowMatrix = new Fraction[length];
            foreach (UIElement rowChild in panel.Children) {
                if (rowChild is TextBox || rowChild is Button) {
                    string text = extractFromButtonOrTextBox(rowChild);

                    Fraction fraction;
                    try {
                        fraction = new Fraction(text);
                    } catch (Exception e) {
                        throw new Exception("Не может быть преобразован в число текст: \"" + text + "\"");
                    }

                    rowMatrix[offset++] = fraction;
                } else {
                    throw new Exception("Неопознаная структура в строке RightBar");
                }
            }

            return rowMatrix;
        }

        private static String extractFromButtonOrTextBox(UIElement element) {
            if (element is TextBox) {
                return ((TextBox) element).Text;
            }

            if (element is Button) {
                return ((Button) element).Content.ToString();
            }

            throw new FormatException("Не удалось прочесть данные из " + element.ToString());
        }

        public static int extractInt(TextBox box, string fieldName = "", bool onlyPositive = false) {
            string text = extractFromButtonOrTextBox(box);
            if (text.Trim().Equals("")) {
                return 0;
            }

            int value;
            try {
                value = Int32.Parse(text);
            } catch (Exception e) {
                throw new FormatException(fieldName + " Не является числом: " + text);
            }

            if (onlyPositive && value < 1) {
                throw new FormatException("Число " + fieldName + " должно быть положительным");
            }

            return value;
        }
    }
}
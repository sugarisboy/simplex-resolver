﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Simplex {
    public class SimplexTable {
        public static string LOG = "";
        
        public const string PATTERN = "{0,8}";

        public Fraction[][] matrix;
        public SimplexTable prevState;
        public string message;
        public bool hasSolve = true;
        public bool finished = false;
        public bool[][] solids;
        public int countSolid = 0;
        public int capacityX = 0;

        public int xa;
        public int xb;

        public SimplexTable(Gause gause, SimplexFunction function) {
            LOG += "SIMPLEX\n";
            Fraction[] args = function.calculateFromGause(gause);
            int gauseRowCount = gause.matrix.Length;
            int gauseColumnCount = gause.matrix[0].Length;
            
            Fraction[][] gauseMatrix = gause.matrix;
            int simplexRowCount = gauseRowCount + 2;
            int simplexColumnCount = gauseColumnCount - gauseRowCount + 1;
            matrix = new Fraction[simplexRowCount][];

            int offsetC = gauseRowCount;
            
            // Первая строка
            Fraction[] firstRow = new Fraction[simplexColumnCount];
            for (int c = 0; c < simplexColumnCount - 2; c++) {
                firstRow[c + 1] = new Fraction(function.columnToX[c + offsetC] + 1, 1);
            }

            firstRow[0] = Fraction.ZERO;
            firstRow[simplexColumnCount - 1] = Fraction.ZERO;
            matrix[0] = firstRow;

            // Средние строки
            for (int r = 0; r < gauseMatrix.Length; r++) {
                Fraction[] row = new Fraction[simplexColumnCount];
                row[0] = new Fraction(function.columnToX[r] + 1, 1);
                for (int c = 0; c < simplexColumnCount - 1; c++) {
                    row[c + 1] = gauseMatrix[r][c + offsetC];
                }

                matrix[r + 1] = row;
            }

            // Последняя строка
            Fraction[] functionRow = new Fraction[simplexColumnCount];
            functionRow[0] = Fraction.ZERO;
            for (int c = 0; c < simplexColumnCount - 2; c++) {
                functionRow[c + 1] = args[function.columnToX[c + offsetC]];
            }

            functionRow[simplexColumnCount - 1] = args[gauseColumnCount - 1].multiple(new Fraction(-1, 1));
            matrix[simplexRowCount - 1] = functionRow;


            this.message = "Исходная симплекс таблица";
        }

        public SimplexTable(SimplexTableHelper helper, SimplexFunction function) {
            LOG += "HELPER\n";
            Fraction[] funcArgs = function.calculateFromHelper(helper);

            matrix = Utils.clone(helper.simplexTable.matrix);
            for (int c = 1; c < matrix[0].Length - 1; c++) {
                matrix[matrix.Length - 1][c] = funcArgs[matrix[0][c].num - 1];
            }

            matrix[matrix.Length - 1][matrix[0].Length - 1] = funcArgs[funcArgs.Length - 1];

            this.matrix = matrix;
            this.capacityX = capacityX;
            this.message = "Симлпекс таблица из вспомогательной задачи";
        }

        public SimplexTable(Fraction[][] matrix) {
            this.matrix = matrix;
            this.capacityX = matrix[0].Length - 2;
            this.message = "Матрица вспомогательной задачи";
        }

        private SimplexTable(Fraction[][] matrix, SimplexTable prevState) {
            this.matrix = matrix;
            this.prevState = prevState;
        }
        
        public SimplexTable swap() {
            if (!hasSolve) {
                throw new Exception("Задача не имеет решения");
            }
            
            if (finished) {
                throw new Exception("Задача завершена");
            }
            
            LOG += "SWAP " + this.xa + " " + this.xb + "\n";
            int a = this.xa;
            int b = this.xb;
            if (a == 0 || b == 0) {
                throw new Exception("Не выбран опорный элемент");
            }

            int rows = matrix.Length; // rows
            int columns = matrix[0].Length; // columns 

            Fraction[][] nextMatrix = Utils.clone(matrix);
            int xa = 0, xb = 0;
            for (int q = 1; q < columns; q++) {
                Fraction e = matrix[0][q];
                if (e.Equals(a) || e.Equals(b)) {
                    xb = q;
                }
            }

            for (int q = 1; q < rows; q++) {
                Fraction e = matrix[q][0];
                if (e.Equals(a) || e.Equals(b)) {
                    xa = q;
                }
            }

            // Меняем иксы
            nextMatrix[0][xb] = matrix[xa][0];
            nextMatrix[xa][0] = matrix[0][xb];

            // Переворачиваем опорный
            nextMatrix[xa][xb] = matrix[xa][xb].flip();

            // по строке
            for (int q = 1; q < columns; q++) {
                if (xb == q)
                    continue;
                nextMatrix[0][q] = matrix[0][q];
                nextMatrix[xa][q] = matrix[xa][q].multiple(nextMatrix[xa][xb]);
            }

            // По столбцу
            for (int q = 1; q < rows; q++) {
                if (xa == q)
                    continue;
                nextMatrix[q][0] = matrix[q][0];
                nextMatrix[q][xb] = matrix[q][xb].multiple(nextMatrix[xa][xb].multiple(new Fraction(-1, 1)));
            }

            for (int j = 1; j < rows; j++) {
                if (xa == j) continue;

                StringBuilder vector1 = new StringBuilder();
                StringBuilder vector2 = new StringBuilder();
                StringBuilder result1 = new StringBuilder();


                Fraction k = matrix[j][xb];
                Console.Out.WriteLine("k = " + k + ":");

                vector1.Append(String.Format(PATTERN + "(", ""));
                vector2.Append(String.Format(k.num < 0 ? PATTERN + "(" : PATTERN + "(",
                    k.multiple(new Fraction(-1, 1))));
                result1.Append(String.Format(PATTERN + "(", ""));

                for (int i = 1; i < columns; i++) {
                    if (xb == i) {
                        vector1.Append(String.Format(PATTERN, "x"));
                        vector2.Append(String.Format(PATTERN, "x"));
                        result1.Append(String.Format(PATTERN, "x"));
                        continue;
                    }

                    nextMatrix[j][i] = matrix[j][i].minus(nextMatrix[xa][i].multiple(k));

                    vector1.Append(String.Format(PATTERN, matrix[j][i]));
                    vector2.Append(String.Format(PATTERN, nextMatrix[xa][i]));
                    result1.Append(String.Format(PATTERN, nextMatrix[j][i]));
                }

                vector1.Append(")");
                vector2.Append(")");
                result1.Append(")");

                Console.Out.WriteLine("");
                Console.Out.WriteLine(vector1);
                Console.Out.WriteLine(vector2);
                Console.Out.WriteLine("-------------------------------------------------------------");
                Console.Out.WriteLine(result1);
                Console.Out.WriteLine("");
            }

            return createState(nextMatrix, "Опорный элемент выбран для Xi = " + a + " и Xj = " + b);
        }

        public bool hasNextStep() {
            return hasSolve && !finished;
        }

        public SimplexTable deleteColumn(int removeColumn) {
            if (removeColumn <= capacityX) {
                return this;
            }
            
            if (removeColumn < 1) {
                throw new Exception("Отрицательный икс");
            }

            int rows = matrix.Length; // rows
            int columns = matrix[0].Length; // columns 

            int xb = 0;
            for (int q = 1; q < columns; q++) {
                Fraction e = matrix[0][q];
                if (e.Equals(removeColumn)) {
                    xb = q;
                }
            }

            if (xb == 0) {
                throw new Exception("Не нашлось икса " + removeColumn);
            }

            Fraction[][] update = new Fraction[rows][];
            for (int j = 0; j < rows; j++) {
                Fraction[] row = new Fraction[columns - 1];
                for (int i = 0; i < columns; i++) {
                    if (i == xb)
                        continue;

                    row[i < xb ? i : i - 1] = matrix[j][i];
                }

                update[j] = row;
            }

            return createState(update, "Удалена колонка для Xi = " + xb);
        }

        public int[] findAutomaticSolid() {
            bool[][] solidElements = findSolidElements();
            for (int r = 0; r < solidElements.Length; r++) {
                for (int c = 0; c < solidElements[0].Length; c++) {
                    if (solidElements[r][c]) {
                        xa = matrix[0][c].num;
                        xb = matrix[r][0].num;
                        return new int[] {xa, xb};
                    }
                }
            }

            return null;
        }

        public bool[][] findSolidElements() {
            if (this.solids == null) {
                countSolid = 0;
                int countHardSolids = 0;
                
                int rows = matrix.Length;
                int columns = matrix[0].Length;

                solids = new bool[rows][];
                bool[][] hardSolids = new bool[rows][];
                for (int i = 0; i < rows; i++) {
                    bool[] row = new bool[columns];
                    bool[] hardRow = new bool[columns];
                    for (int j = 0; j < columns; j++) {
                        row[j] = false;
                        hardRow[j] = false;
                    }

                    solids[i] = row;
                    hardSolids[i] = hardRow;
                }

                for (int c = 1; c < columns - 1; c++) {
                    bool hasPositive = false;
                    for (int r = 1; r < rows - 1; r++) {
                        if (matrix[r][c].num > 0)
                            hasPositive = true;
                    }

                    if (!hasPositive && matrix[matrix.Length - 1][c].toDouble() <= 0) {
                        hasSolve = false;
                        //throw new Exception("Симплекс таблица не имеет решений, так как функция обращена в ноль");
                    }


                    if (matrix[rows - 1][c].num < 0) {
                        Fraction minXc = new Fraction(999999, 1);
                        Fraction minValue = new Fraction(999999, 1);

                        for (int r = 1; r < rows; r++) {
                            Fraction xC = matrix[r][c];
                            if (xC.toDouble() > 0) {
                                Fraction Xi = matrix[r][columns - 1].divide(xC);
                                if (Xi.toDouble() < minValue.toDouble()) {
                                    minValue = Xi;
                                }
                            }
                        }

                        for (int r = 1; r < rows; r++) {
                            Fraction xC = matrix[r][c];
                            if (xC.toDouble() > 0) {
                                Fraction Xi = matrix[r][columns - 1].divide(xC);
                                if (Xi.Equals(minValue) /*&& matrix[r][0].num > capacityX*/) {
                                    if (matrix[r][0].num > capacityX) {
                                        hardSolids[r][c] = true;
                                        countHardSolids++;
                                    } else {
                                        solids[r][c] = true;
                                        countSolid++;
                                    }
                                }
                            }
                        }
                    }
                }

                if (countSolid == 0) {
                    solids = hardSolids;
                    countSolid = countHardSolids;
                }
                
                if (countSolid == 0) {
                    finished = true;
                }
            }
            
            return solids;
        }

        private SimplexTable createState(Fraction[][] matrix, string message = "") {
            SimplexTable table = new SimplexTable(matrix, this);
            table.message = message;
            table.capacityX = capacityX;
            return table;
        }
    }
}
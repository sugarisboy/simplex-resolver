﻿using System;
using System.Windows.Navigation;

namespace Simplex {
    public class SimplexFunction {
        public Fraction[] argsMin;
        public Fraction[] args;
        public FunctionType type;
        public int[] columnToX;

        public enum FunctionType {
            MAX,
            MIN
        }

        public SimplexFunction(Fraction[] args, FunctionType type) {
            this.argsMin = Utils.clone(new Fraction[1][]{args})[0];
            this.args = args;
            this.type = type;
            
            normalize();
        }

        public void normalize() {
            if (type == FunctionType.MAX) {
                for (int i = 0; i < argsMin.Length; i++) {
                    argsMin[i] = argsMin[i].multiple(new Fraction(-1, 1));
                }
            }
        }

        public Fraction[] calculateFromGause(Gause gause) {
            Fraction[][] matrix = gause.matrix;
            Fraction[] basis = gause.basis;

            int countNonZeroInBasis = 0;
            for (int i = 0; i < basis.Length; i++) {
                if (!basis[i].Equals(0))
                    countNonZeroInBasis++;
            }

            int indexFromBasis = 0;
            int indexNoneBasis = countNonZeroInBasis;

            columnToX = new int[basis.Length];
            for (int i = 0; i < basis.Length; i++) {
                if (basis[i].Equals(0)) {
                    columnToX[indexNoneBasis++] = i;
                } else {
                    columnToX[indexFromBasis++] = i;
                }
            }

            Fraction[][] unnormalized = Utils.clone(matrix);
            for (int c = 0; c < basis.Length; c++) {
                int x = columnToX[c]; //X_i
                for (int r = 0; r < matrix.Length; r++) {
                    unnormalized[r][x] = matrix[r][c];
                }
            }

            Fraction[][] xFromOtherX = new Fraction[basis.Length][];
            for (int r = 0; r < matrix.Length; r++) {
                int x = columnToX[r];
                Fraction[] args = new Fraction[basis.Length + 1];
                for (int c = 0; c < basis.Length; c++) {
                    args[c] = basis[c].Equals(Fraction.ZERO)
                        ? unnormalized[r][c].multiple(new Fraction(-1, 1))
                        : Fraction.ZERO;
                }

                args[basis.Length] = unnormalized[r][basis.Length];
                xFromOtherX[x] = args;
            }

            Fraction[] xIFuncArgs = new Fraction[basis.Length + 1];
            for (int xi = 0; xi < basis.Length; xi++) {
                Fraction arg = Fraction.ZERO;
                if (basis[xi].Equals(Fraction.ZERO)) {
                    arg = arg.sum(this.argsMin[xi]);
                    for (int c = 0; c < basis.Length; c++) {
                        if (xFromOtherX[c] != null) {
                            arg = arg.sum(this.argsMin[c].multiple(xFromOtherX[c][xi]));
                        }
                    }
                }

                xIFuncArgs[xi] = arg;
            }

            Fraction argB = Fraction.ZERO;
            for (int c = 0; c < basis.Length; c++) {
                if (xFromOtherX[c] != null) {
                    argB = argB.sum(this.argsMin[c].multiple(xFromOtherX[c][basis.Length]));
                }
            }

            xIFuncArgs[basis.Length] = argB;


            return xIFuncArgs;
        }

        public Fraction[] calculateFromHelper(SimplexTableHelper helper) {
            var matrix = helper.simplexTable.matrix;
            int rows = matrix.Length;
            int columns = matrix[0].Length;
            int countX = rows + columns - 4;
            Fraction[][] xIArgsFunc = new Fraction[countX][];

            // Расчитываем аргументы для x_i
            for (int r = 1; r < rows-1; r++) {
                Fraction[] row = new Fraction[countX + 1];
                for (int c = 0; c < countX; c++) {
                    row[c] = Fraction.ZERO;
                }

                for (int c = 1; c < matrix[0].Length - 1; c++) {
                    row[matrix[0][c].num - 1] = matrix[r][c].multiple(new Fraction(-1, 1));
                }

                xIArgsFunc[matrix[r][0].num - 1] = row;
                xIArgsFunc[matrix[r][0].num - 1][countX] = matrix[r][matrix[0].Length - 1];
            }

            // Расчитываем итоговые аргументы для функции
            Fraction[] funcArgs = new Fraction[countX + 1];

            for (int i = 0; i < funcArgs.Length; i++)
                funcArgs[i] = Fraction.ZERO;

            for (int c = 1; c < columns - 1; c++) {
                int xi = matrix[0][c].num;
                Fraction sum = Fraction.ZERO;
                for (int i = 0; i < countX; i++) {
                    if (xIArgsFunc[i] != null) {
                        sum = sum.sum(xIArgsFunc[i][xi - 1].multiple(argsMin[i]));
                    }
                }

                sum = sum.sum(argsMin[xi - 1]);

                funcArgs[xi - 1] = sum;
            }

            int b = matrix[0][columns - 1].num;
            Fraction sumB = Fraction.ZERO;
            for (int i = 0; i < countX; i++) {
                if (xIArgsFunc[i] != null) {
                    sumB = sumB.sum(xIArgsFunc[i][countX].multiple(argsMin[i]));
                }
            }

            funcArgs[funcArgs.Length - 1] = sumB.multiple(new Fraction(-1, 1));
            
            return funcArgs;
        }
    }
}
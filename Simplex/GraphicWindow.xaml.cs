﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using Color = System.Drawing.Color;

namespace Simplex {
    public partial class GraphicWindow : Window {
        public SimplexTable table;
        public Dictionary<Fraction, Fraction[]> values = new Dictionary<Fraction, Fraction[]>();
        public static GraphicWindow instance;

        public GraphicWindow(SimplexTable table) {
            if (instance != null && instance.ShowActivated) {
                instance.Close();
            }
            instance = this;
            this.table = table;

            InitializeComponent();
            renderLeftBar();
            readSimplexTable();
        }

        public StackPanel functionPanel;
        public StackPanel scopePanel;
        public StackPanel leftBar;
        public StackPanel rightBar;

        public Fraction[][] scopeMatrix;
        public SimplexFunction function;
        public Label label;

        public void readSimplexTable() {
            if (table == null) {
                this.Show();
                return;
            }

            Fraction[][] matrix = table.matrix;

            int countX = matrix[0].Length - 2;
            int countScopes = matrix.Length - 2;

            if (countX > 2) {
                MainWindow.get().statusBar.Content = "Задача не может быть решена графически";
                return;
            }
            
            Button addRow = new Button();
            addRow.Content = "+";

            scopePanel.Children.Clear();
            for (int i = 0; i < countScopes; i++) {
                changeRow(addRow, null);
            }

            ((TextBox) functionPanel.Children[0]).Text = matrix[matrix.Length - 1][1]
                .multiple(Fraction.MINUS_ONE).ToString();
            if (countX == 1) {
                ((TextBox) functionPanel.Children[2]).Text = "0";
                ((TextBox) functionPanel.Children[4]).Text = matrix[matrix.Length - 1][2]
                    .multiple(Fraction.MINUS_ONE).ToString();
            } else if (countX == 2) {
                ((TextBox) functionPanel.Children[2]).Text = matrix[matrix.Length - 1][2]
                    .multiple(Fraction.MINUS_ONE).ToString();
                ((TextBox) functionPanel.Children[4]).Text = matrix[matrix.Length - 1][3]
                    .multiple(Fraction.MINUS_ONE).ToString();
            }

            ((ComboBox) functionPanel.Children[6]).Text = SimplexCore.get().function.type.ToString().ToLower();

            for (int r = 0; r < countScopes; r++) {
                ((TextBox) ((StackPanel) scopePanel.Children[r]).Children[0]).Text = matrix[r + 1][1].ToString();
                if (countX == 1) {
                    ((TextBox) ((StackPanel) scopePanel.Children[r]).Children[2]).Text = "0";
                    ((TextBox) ((StackPanel) scopePanel.Children[r]).Children[5]).Text = matrix[r + 1][2].ToString();
                } else if (countX == 2) {
                    ((TextBox) ((StackPanel) scopePanel.Children[r]).Children[2]).Text = matrix[r + 1][2].ToString();
                    ((TextBox) ((StackPanel) scopePanel.Children[r]).Children[5]).Text = matrix[r + 1][3].ToString();
                }
            }
            
            this.Show();
        }

        public void renderLeftBar() {
            Window.Children.Clear();
            leftBar?.Children.Clear();
            rightBar?.Children.Clear();

            StackPanel panel = new StackPanel();
            panel.Orientation = Orientation.Horizontal;
            panel.Margin = new Thickness(10, 10, 10, 10);
            leftBar = new StackPanel();
            rightBar = new StackPanel();

            Label titleFunction = new Label();
            titleFunction.Content = "Функция: ";
            // рендер функции
            if (functionPanel == null) {
                functionPanel = new StackPanel();
                functionPanel.Orientation = Orientation.Horizontal;

                TextBox xBox = new TextBox();
                xBox.Width = 40;
                xBox.Height = 20;
                xBox.TextAlignment = TextAlignment.Right;

                Label labelX = new Label();
                labelX.Content = "x +";

                TextBox yBox = new TextBox();
                yBox.Width = 40;
                yBox.Height = 20;
                yBox.TextAlignment = TextAlignment.Right;

                Label labelY = new Label();
                labelY.Content = "y -";

                TextBox cBox = new TextBox();
                cBox.Text = "0";
                cBox.Width = 40;
                cBox.Height = 20;

                Label labelType = new Label();
                labelType.Content = "→";

                ComboBox functionTypeBox = new ComboBox();
                TextBlock minType = new TextBlock();
                minType.Text = "min";
                TextBlock maxType = new TextBlock();
                maxType.Text = "max";
                functionTypeBox.Items.Add(minType);
                functionTypeBox.Items.Add(maxType);
                functionTypeBox.SelectedItem = minType;

                functionPanel.Children.Add(xBox);
                functionPanel.Children.Add(labelX);
                functionPanel.Children.Add(yBox);
                functionPanel.Children.Add(labelY);
                functionPanel.Children.Add(cBox);
                functionPanel.Children.Add(labelType);
                functionPanel.Children.Add(functionTypeBox);
            }

            leftBar.Children.Add(titleFunction);
            leftBar.Children.Add(functionPanel);

            // Рендер ограничений
            {
                Label title = new Label();
                title.Content = "Ограничения: ";
                leftBar.Children.Add(title);

                Button addRow = new Button();
                addRow.Content = "+";
                addRow.Width = 20;
                addRow.Click += this.changeRow;

                Button deleteRow = new Button();
                deleteRow.Content = "-";
                deleteRow.Width = 20;
                deleteRow.Margin = new Thickness(4, 0, 0, 0);
                deleteRow.Click += this.changeRow;

                if (scopePanel == null) {
                    changeRow(addRow, null);
                }

                leftBar.Children.Add(scopePanel);

                StackPanel rowChangePanel = new StackPanel();
                rowChangePanel.Orientation = Orientation.Horizontal;

                rowChangePanel.Children.Add(addRow);
                rowChangePanel.Children.Add(deleteRow);
                leftBar.Children.Add(rowChangePanel);
            }

            // Кнопка расчета
            {
                Button applyButton = new Button();
                applyButton.Content = "Готово";
                applyButton.Width = 80;
                applyButton.Margin = new Thickness(4, 0, 0, 0);
                applyButton.Click += this.calculateClick;
                leftBar.Children.Add(applyButton);
            }

            Label normal = new Label();
            normal.Foreground = Brushes.Fuchsia;
            normal.Content = "Нормаль";
            leftBar.Children.Add(normal);


            Label minusNormal = new Label();
            minusNormal.Foreground = Brushes.Firebrick;
            minusNormal.Content = "Обратная нормаль";
            leftBar.Children.Add(minusNormal);


            Label lineLevel = new Label();
            lineLevel.Foreground = Brushes.Indigo;
            lineLevel.Content = "Линия уровня";
            leftBar.Children.Add(lineLevel);

            if (label == null) {
                label = new Label();
                label.Content = "";
            }

            leftBar.Children.Add(label);

            // Рендер графика

            panel.Children.Add(leftBar);
            {
                Chart chart = new Chart();
                ChartArea area = new ChartArea("Default");
                chart.Width = 700;
                chart.Height = 700;
                chart.ChartAreas.Add(area);

                // Рендер ограничений и фигуры
                foreach (var series in renderGraphic()) {
                    chart.Series.Add(series);
                }

                // Ось X
                Series xAxis = new Series("xAxis");
                xAxis.ChartArea = "Default";
                xAxis.ChartType = SeriesChartType.Line;
                xAxis.Color = Color.Black;
                xAxis.BorderWidth = 5;

                for (int i = -2; i <= 13; i++) {
                    xAxis.Points.Add(new DataPoint(i, 0));
                }

                chart.Series.Insert(0, xAxis);

                // Ось Y
                Series yAxis = new Series("yAxis");
                yAxis.ChartArea = "Default";
                yAxis.ChartType = SeriesChartType.Line;
                yAxis.Color = Color.Black;
                yAxis.BorderWidth = 5;

                yAxis.Points.Add(new DataPoint(-1, -99999999));
                yAxis.Points.Add(new DataPoint(0, 0));
                yAxis.Points.Add(new DataPoint(1, 99999999));
                chart.Series.Insert(0, yAxis);

                // Фигура
                /*Series figure = new Series("figure");
                figure.ChartArea = "Default";
                figure.BorderWidth = 3;
                figure.ChartType = SeriesChartType.Line;
                
                figure.Points.Add(new DataPoint(1, 4));
                figure.Points.Add(new DataPoint(4, 1));
                figure.Points.Add(new DataPoint(1, 1));
                figure.Points.Add(new DataPoint(1, 4));

                chart.Series.Add(figure);*/

                // Кофиг
                area.AxisY.Minimum = -2;
                area.AxisY.Maximum = 13;
                area.AxisX.Minimum = -2;
                area.AxisX.Maximum = 13;
                area.AxisX.Interval = 1;
                area.AxisY.Interval = 1;

                WindowsFormsHost formsHost = new WindowsFormsHost();
                formsHost.Child = chart;

                rightBar.Children.Add(formsHost);
            }

            panel.Children.Add(rightBar);
            Window.Children.Add(panel);
        }

        public List<Series> renderGraphic() {
            List<Series> series = new List<Series>();

            if (function != null) {
                Series normal = new Series("normal");
                normal.ChartArea = "Default";
                normal.BorderWidth = 2;
                normal.Color = Color.Fuchsia;
                normal.ChartType = SeriesChartType.Line;

                for (int x = 0; x <= 2; x++) {
                    Fraction a = function.args[0];
                    Fraction b = function.args[1];

                    normal.Points.Add(new DataPoint(a.toDouble() * x, b.toDouble() * x));
                }

                series.Add(normal);

                if (function.type.Equals(SimplexFunction.FunctionType.MIN)) {
                    Series minusNormal = new Series("minusNormal");
                    minusNormal.ChartArea = "Default";
                    minusNormal.BorderWidth = 2;
                    minusNormal.Color = Color.Firebrick;
                    minusNormal.ChartType = SeriesChartType.Line;

                    for (int x = 0; x <= 2; x++) {
                        Fraction a = function.args[0];
                        Fraction b = function.args[1];

                        minusNormal.Points.Add(new DataPoint(-a.toDouble() * x, -b.toDouble() * x));
                    }

                    series.Add(minusNormal);
                }

                Series lineLevel = new Series("lineLevel");
                lineLevel.ChartArea = "Default";
                lineLevel.BorderWidth = 2;
                lineLevel.Color = Color.Indigo;
                lineLevel.ChartType = SeriesChartType.Line;

                Fraction funcA = function.args[0];
                Fraction funcB = function.args[1];
                Fraction funcC = Fraction.ZERO; //function.args[2];

                if (funcB.Equals(Fraction.ZERO) && !funcA.Equals(Fraction.ZERO)) {
                    // x = c / a
                    Fraction cdividea = funcC.divide(funcA);
                    lineLevel.Points.Add(new DataPoint(cdividea.toDouble() - 0.000001D, -999999));
                    lineLevel.Points.Add(new DataPoint(cdividea.toDouble() + 0.000001D, 999999));
                } else {
                    // (c - ax) / b
                    for (int x = -2; x <= 13; x++) {
                        Fraction ax = funcA.multiple(new Fraction(x, 1));
                        Fraction cminusax = funcC.minus(ax);
                        Fraction divideb = funcB.toDouble() == 0 ? cminusax : cminusax.divide(funcB);
                        lineLevel.Points.Add(new DataPoint(x, divideb.toDouble()));
                    }
                }

                series.Add(lineLevel);
            }

            if (scopeMatrix != null) {
                int countScopes = scopePanel.Children.Count;
                for (int i = 0; i < countScopes; i++) {
                    Fraction a = scopeMatrix[i][0];
                    Fraction b = scopeMatrix[i][1];
                    Fraction c = scopeMatrix[i][2];

                    Series function = new Series("function" + i);
                    function.ChartArea = "Default";
                    function.BorderWidth = 3;
                    function.ChartType = SeriesChartType.Line;

                    if (b.Equals(Fraction.ZERO) && !a.Equals(Fraction.ZERO)) {
                        // x = c / a
                        Fraction cdividea = c.divide(a);
                        function.Points.Add(new DataPoint(cdividea.toDouble() - 0.000001D, -999999));
                        function.Points.Add(new DataPoint(cdividea.toDouble() + 0.000001D, 999999));
                    } else {
                        // (c - ax) / b
                        for (int x = -2; x <= 13; x++) {
                            Fraction ax = a.multiple(new Fraction(x, 1));
                            Fraction cminusax = c.minus(ax);
                            Fraction divideb = b.toDouble() == 0 ? cminusax : cminusax.divide(b);
                            function.Points.Add(new DataPoint(x, divideb.toDouble()));
                        }
                    }


                    series.Add(function);
                }

                Series hatch = new Series("fill");
                hatch.ChartArea = "Default";
                hatch.BorderWidth = 1;
                hatch.ChartType = SeriesChartType.FastLine;
                series.Insert(0, hatch);

                SimplexFunction.FunctionType type = function.type;

                Fraction extrX = null;
                Fraction extrY = null;
                Fraction extrValue = null;

                values.Clear();
                for (Fraction _x = Fraction.ZERO; _x.toDouble() <= 13; _x = _x.sum(new Fraction(1, 20))) {
                    for (Fraction _y = Fraction.ZERO; _y.toDouble() <= 13; _y = _y.sum(new Fraction(1, 20))) {
                        bool isContains = true;

                        double x = _x.toDouble();
                        double y = _y.toDouble();

                        for (int i = 0; i < countScopes; i++) {
                            Fraction a = scopeMatrix[i][0];
                            Fraction b = scopeMatrix[i][1];
                            Fraction c = scopeMatrix[i][2];

                            Fraction ax = a.multiple(new Fraction((int) (x * 100), 100));
                            Fraction by = b.multiple(new Fraction((int) (y * 100), 100));
                            Fraction axplusby = ax.sum(by);

                            if (axplusby.toDouble() > c.toDouble()) {
                                isContains = false;
                                break;
                            }
                        }

                        if (isContains) {
                            hatch.Points.Add(new DataPoint(x, y));

                            Fraction fractionX = new Fraction((int) (x * 100), 100);
                            Fraction fractionY = new Fraction((int) (y * 100), 100);
                            Fraction ax = function.args[0].multiple(fractionX);
                            Fraction by = function.args[1].multiple(fractionY);
                            Fraction axplusby = ax.sum(by);

                            values.Add(axplusby, new[] {ax, by});

                            bool ifMaxAndMore = type.Equals(SimplexFunction.FunctionType.MAX) &&
                                                extrValue != null &&
                                                axplusby.toDouble() > extrValue.toDouble();
                            bool ifMinAndLess = type.Equals(SimplexFunction.FunctionType.MIN) &&
                                                extrValue != null &&
                                                axplusby.toDouble() < extrValue.toDouble();

                            if (extrY == null || ifMaxAndMore || ifMinAndLess) {
                                extrX = fractionX;
                                extrY = fractionY;
                                extrValue = axplusby;
                            }
                        }
                    }
                }

                /*
                if (extrX != null) {
                    label.Content = $"F({extrX}, {extrY}) = {extrValue.minus(function.args[2])}";
                }*/
            }

            return series;
        }


        private void calculateClick(object sender, RoutedEventArgs e) {
            bool isread = readData();
            if (isread) {
                renderLeftBar();
                findResolve();
            }
        }

        private void findResolve() {
            int rowCount = scopePanel.Children.Count;

            try {
                Fraction[][] matrix = new Fraction[rowCount][];
                for (int row = 0; row < rowCount; row++) {
                    Fraction[] matrixRow = new Fraction[3 + rowCount];

                    for (int c = 0; c < 3 + rowCount; c++)
                        matrixRow[c] = Fraction.ZERO;

                    matrixRow[0] = scopeMatrix[row][0];
                    matrixRow[1] = scopeMatrix[row][1];
                    matrixRow[2 + row] = Fraction.ONE;
                    matrixRow[2 + rowCount] = scopeMatrix[row][2];

                    matrix[row] = matrixRow;
                }

                Fraction[] basis = new Fraction[rowCount + 2];
                basis[0] = Fraction.ZERO;
                basis[1] = Fraction.ZERO;
                for (int i = 2; i < rowCount + 2; i++) {
                    basis[i] = Fraction.ONE;
                }

                Gause gause = new Gause(matrix, basis).calculate();

                //SimplexTable table = new SimplexTable(helper, function);
                SimplexTable table = this.table ?? new SimplexTable(gause, function);
                table.findAutomaticSolid();
                while (!(table.finished || !table.hasSolve)) {
                    table = table.swap();
                    table.findAutomaticSolid();
                }

                if (!table.hasSolve) {
                    label.Content = "Нет решения (2)";
                    return;
                }

                Fraction answerX = null, answerY = null, answerValue = null;
                for (int r = 1; r < table.matrix.Length - 1; r++) {
                    if (table.matrix[r][0].toDouble() == 1) {
                        answerX = table.matrix[r][table.matrix[0].Length - 1];
                    } else if (table.matrix[r][0].toDouble() == 2) {
                        answerY = table.matrix[r][table.matrix[0].Length - 1];
                    }
                }

                answerValue = table.matrix[table.matrix.Length - 1][table.matrix[0].Length - 1];

                if (values.ContainsKey(answerValue)) {
                    answerX = values[answerValue][0];
                    answerY = values[answerValue][1];
                }

                if (values.Count != 0) {
                    label.Content = "F(" + (answerX ?? Fraction.ZERO) + ", " +
                                    (answerY ?? Fraction.ZERO) + ") = " +
                                    (answerValue
                                        .multiple(function.type.Equals(SimplexFunction.FunctionType.MIN)
                                            ? Fraction.MINUS_ONE
                                            : Fraction.ONE
                                        ) ?? Fraction.ZERO);
                } else {
                    label.Content = "Нет решения (4)";
                }
            } catch (Exception e) {
                label.Content = "Нет решения (3)";
            }
        }

        private void changeRow(object sender, RoutedEventArgs e) {
            if (scopePanel == null) {
                scopePanel = new StackPanel();
            }

            if (sender is Button) {
                Button button = (Button) sender;
                if (button.Content.Equals("+")) {
                    StackPanel rowPanel = new StackPanel();
                    rowPanel.Orientation = Orientation.Horizontal;

                    TextBox xBox = new TextBox();
                    xBox.Width = 50;
                    xBox.Height = 20;
                    xBox.TextAlignment = TextAlignment.Right;

                    Label labelX = new Label();
                    labelX.Content = "x + ";

                    TextBox yBox = new TextBox();
                    yBox.Width = 50;
                    yBox.Height = 20;
                    yBox.TextAlignment = TextAlignment.Right;

                    Label labelY = new Label();
                    labelY.Content = "y ";

                    ComboBox sign = new ComboBox();
                    TextBlock lessBlock = new TextBlock();
                    lessBlock.Text = "≤";
                    TextBlock moreBlock = new TextBlock();
                    moreBlock.Text = "≥";
                    sign.Items.Add(lessBlock);
                    sign.Items.Add(moreBlock);
                    sign.SelectedItem = lessBlock;

                    TextBox cBox = new TextBox();
                    cBox.Width = 50;
                    cBox.Height = 20;
                    cBox.Margin = new Thickness(4, 0, 0, 0);

                    rowPanel.Children.Add(xBox);
                    rowPanel.Children.Add(labelX);
                    rowPanel.Children.Add(yBox);
                    rowPanel.Children.Add(labelY);
                    rowPanel.Children.Add(sign);
                    rowPanel.Children.Add(cBox);
                    scopePanel.Children.Add(rowPanel);
                } else {
                    scopePanel.Children.RemoveAt(scopePanel.Children.Count - 1);
                }
            }
        }

        private bool readData() {
            int countScopes = scopePanel.Children.Count;

            Fraction[] args;
            try {
                args = new Fraction[] {
                    new Fraction(((TextBox) functionPanel.Children[0]).Text),
                    new Fraction(((TextBox) functionPanel.Children[2]).Text),
                    new Fraction(((TextBox) functionPanel.Children[4]).Text),
                    Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO,
                    Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO,
                    Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO,
                    Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO,
                    Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO,
                    Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO, Fraction.ZERO,
                };
            } catch (FormatException e) {
                label.Content = "Ошибка прочтения функции";
                return false;
            }

            SimplexFunction.FunctionType minOrMax = ((ComboBox) functionPanel.Children[6]).Text.ToLower().Equals("min")
                ? SimplexFunction.FunctionType.MIN
                : SimplexFunction.FunctionType.MAX;

            function = new SimplexFunction(args, minOrMax);

            scopeMatrix = new Fraction[countScopes][];
            for (int i = 0; i < countScopes; i++) {
                try {
                    StackPanel rowPanel = (StackPanel) scopePanel.Children[i];
                    Fraction[] row = new Fraction[3];

                    ComboBox signBox = (ComboBox) rowPanel.Children[4];
                    string signBoxText = signBox.Text;
                    int k = signBoxText.Equals("≤") ? 1 : -1;

                    row[0] = new Fraction(((TextBox) rowPanel.Children[0]).Text).multiple(new Fraction(k, 1));
                    row[1] = new Fraction(((TextBox) rowPanel.Children[2]).Text).multiple(new Fraction(k, 1));
                    row[2] = new Fraction(((TextBox) rowPanel.Children[5]).Text).multiple(new Fraction(k, 1));

                    scopeMatrix[i] = row;
                } catch (Exception e) {
                    label.Content = "Ошибка прочтения ограничения №" + (i + 1);
                    return false;
                }
            }

            return true;
        }
    }
}
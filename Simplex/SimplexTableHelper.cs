﻿using System.Windows.Media;

namespace Simplex {
    public class SimplexTableHelper {

        public SimplexTable simplexTable;
        public SimplexTableHelper prevState;
        
        public SimplexTableHelper(Fraction[][] matrix) {
            int rows = matrix.Length + 2;
            int columns = matrix[0].Length + 1;

            Fraction[][] helpMatrix = new Fraction[rows][];

            // Первая строка
            Fraction[] firstRow = new Fraction[columns];
            for (int c = 0; c < columns - 1; c++) {
                firstRow[c] = new Fraction(c, 1);
            }

            firstRow[columns - 1] = Fraction.ZERO;
            helpMatrix[0] = firstRow;

            // Средние строки
            for (int r = 1; r < rows - 1; r++) {
                Fraction[] row = new Fraction[columns];
                row[0] = new Fraction(columns - 2 + r, 1);
                for (int c = 0; c < columns - 1; c++) {
                    row[c + 1] = matrix[r - 1][c];
                }

                helpMatrix[r] = row;
            }
            
            // Последняя строка
            Fraction[] lastRow = new Fraction[columns];
            lastRow[0] = new Fraction(0, 1);
            for (int c = 1; c < columns; c++) {
                Fraction sum = Fraction.ZERO;
                for (int r = 1; r < rows - 1; r++) {
                    sum = sum.sum(helpMatrix[r][c]);
                }

                sum = sum.multiple(new Fraction(-1, 1));
                lastRow[c] = sum;
            }

            helpMatrix[rows - 1] = lastRow;

            simplexTable = new SimplexTable(helpMatrix);
        }

        public SimplexTableHelper swap() {
            SimplexTableHelper helperWithRemovedColumn = createState(simplexTable.swap().deleteColumn(simplexTable.xb));
            return helperWithRemovedColumn;
        }
        
        private SimplexTableHelper(SimplexTable simplexTable) {
            this.simplexTable = simplexTable;
        }

        private SimplexTableHelper createState(SimplexTable simplexTable) {
            SimplexTableHelper simplexTableHelper = new SimplexTableHelper(simplexTable);
            simplexTableHelper.prevState = this;
            return simplexTableHelper;
        }
    }
}
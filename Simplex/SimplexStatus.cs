﻿namespace Simplex {
    public enum SimplexStatus {
        
        INPUT,
        GAUSE,
        HELPER,
        SIMPLEX,
        RESULT
    }
}
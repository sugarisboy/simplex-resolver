﻿namespace Simplex {
    public class MatrixWithDescription {

        public Fraction[][] matrix;
        public string description;
        public SimplexStatus status;

        public MatrixWithDescription(Fraction[][] matrix, string description, SimplexStatus status) {
            this.matrix = matrix;
            this.description = description;
            this.status = status;
        }
    }
}
﻿using System;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using Microsoft.Win32;

namespace Simplex {
    public partial class MainWindow : Window {
        private static MainWindow instance;

        public GraphicWindow graphicWindow;
        
        public MainWindow() {
            InitializeComponent();
            instance = this;

            renderBasis();
            renderMatrix();
            renderFunction();
        }

        private void ButtonUpdateColumnAndRowClick(object sender, RoutedEventArgs e) {
            int rowCount = Utils.readInt32(rowBox.Text, statusBar);
            int columnCount = Utils.readInt32(columnBox.Text, statusBar);
            graphicWindow?.Close();
            SimplexCore.get().updateRowAndColumn(rowCount, columnCount);
            renderBasis();
            renderMatrix();
            renderFunction();
        }

        public void renderMatrix() {
            RightBar.Children.Clear();

            MatrixWithDescription matrixWithDescription = SimplexCore.get().getMatrix();
            SimplexStatus status = matrixWithDescription.status;
            StepDescription.Content = matrixWithDescription.description;
            Fraction[][] matrix = matrixWithDescription.matrix;

            if (status == SimplexStatus.RESULT) {
                string text = "";
                Fraction[] vector = new Fraction[SimplexCore.get().columnCount - 1];
                for (int i = 0; i < vector.Length; i++)
                    vector[i] = Fraction.ZERO;

                for (int r = 1; r < matrix.Length - 1; r++) {
                    vector[matrix[r][0].num - 1] = matrix[r][matrix[0].Length - 1];
                }

                Fraction extr = matrix[matrix.Length - 1][matrix[0].Length - 1].multiple(
                    SimplexCore.get().function.type == SimplexFunction.FunctionType.MAX
                        ? new Fraction(1, 1)
                        : new Fraction(-1, 1)
                );

                text += "x = (";
                for (int i = 0; i < vector.Length - 1; i++)
                    text += vector[i].ToString() + ", ";
                text += vector[vector.Length - 1] + ") \n \n";

                text += "extr = " + extr.ToString();

                Label label = new Label();
                label.Content = text;
                RightBar.Children.Add(label);
            } else {
                for (int r = 0; r < matrix.Length; r++) {
                    StackPanel stackLine = new StackPanel();
                    stackLine.Orientation = Orientation.Horizontal;

                    for (int c = 0; c < matrix[r].Length; c++) {
                        if (SimplexCore.get().status == SimplexStatus.INPUT) {
                            TextBox box = new TextBox();
                            box.Text = matrix[r][c].Equals(Fraction.ZERO) ? "0" : matrix[r][c].ToString();
                            box.Width = 65;
                            box.Height = 24;
                            box.FontSize = 18;
                            stackLine.Children.Add(box);
                        } else if (status == SimplexStatus.GAUSE) {
                            Button box = new Button();
                            box.Content = matrix[r][c].ToString();
                            box.Width = 65;
                            box.Height = 24;
                            box.FontSize = 18;
                            stackLine.Children.Add(box);
                        } else if (status == SimplexStatus.SIMPLEX) {
                            bool isSolid = SimplexCore.get().simplexTable.findSolidElements()[r][c];
                            Button box = new Button();

                            if (isSolid && SimplexCore.get().simplexTable.hasSolve) {
                                int xa = matrix[0][c].num;
                                int xb = matrix[r][0].num;
                                bool isSelected = SimplexCore.get().simplexTable.xa == xa &&
                                                  SimplexCore.get().simplexTable.xb == xb;
                                box.Background = isSelected ? Brushes.Coral : Brushes.CornflowerBlue;
                                box.Name = "_" + matrix[0][c] + "and" + matrix[r][0];
                                box.Click += ButtonSolidSetClick;
                            }

                            box.Content = matrix[r][c].ToString();
                            box.Width = 65;
                            box.Height = 24;
                            box.FontSize = 18;
                            stackLine.Children.Add(box);
                        } else if (status == SimplexStatus.HELPER) {
                            bool isSolid = SimplexCore.get().helper.simplexTable.findSolidElements()[r][c];
                            Button box = new Button();

                            if (isSolid && SimplexCore.get().helper.simplexTable.hasSolve) {
                                int xa = matrix[0][c].num;
                                int xb = matrix[r][0].num;
                                bool isSelected = SimplexCore.get().helper.simplexTable.xa == xa &&
                                                  SimplexCore.get().helper.simplexTable.xb == xb;
                                box.Background = isSelected ? Brushes.Coral : Brushes.CornflowerBlue;
                                box.Name = "_" + matrix[0][c] + "and" + matrix[r][0];
                                box.Click += ButtonSolidSetClick;
                            }

                            box.Content = matrix[r][c].ToString();
                            box.Width = 65;
                            box.Height = 24;
                            box.FontSize = 18;
                            stackLine.Children.Add(box);
                        }
                    }

                    RightBar.Children.Add(stackLine);
                }
            }
        }

        public void renderBasis() {
            Fraction[] basis = SimplexCore.get().basis;
            BasisPanel.Children.Clear();
            if (CheckBoxBasis.IsChecked == true) {
                StackPanel stackLine = new StackPanel();
                stackLine.Orientation = Orientation.Horizontal;

                for (int c = 0; c < SimplexCore.get().columnCount - 1; c++) {
                    TextBox box = new TextBox();
                    box.Text = basis[c] == null ? "0" : basis[c].ToString();
                    box.Width = 15;
                    box.Height = 24;
                    box.FontSize = 18;
                    stackLine.Children.Add(box);
                }

                BasisPanel.Children.Add(stackLine);
            }
        }

        public void renderFunction() {
            SimplexFunction function = SimplexCore.get().function;
            Fraction[] functionArgs = function.args;
            FunctionPanel.Children.Clear();
            FunctionPanel.Orientation = Orientation.Horizontal;
            StackPanel stackLine = new StackPanel();
            stackLine.Orientation = Orientation.Horizontal;

            for (int c = 0; c < SimplexCore.get().columnCount - 1; c++) {
                TextBox box = new TextBox();
                box.Text = functionArgs[c].ToString();
                box.Width = 65;
                box.Height = 24;
                box.FontSize = 18;
                stackLine.Children.Add(box);
            }

            ComboBox item = new ComboBox();
            TextBlock minBlock = new TextBlock();
            minBlock.Text = "min";
            TextBlock maxBlock = new TextBlock();
            maxBlock.Text = "max";
            if (function.type == SimplexFunction.FunctionType.MAX) {
                item.SelectedItem = maxBlock;
            } else {
                item.SelectedItem = minBlock;
            }

            item.Items.Add(minBlock);
            item.Items.Add(maxBlock);

            FunctionPanel.Children.Add(stackLine);
            FunctionPanel.Children.Add(item);
        }

        public static MainWindow get() {
            return instance;
        }

        private void ButtonNext(object sender, RoutedEventArgs e) {
            try {
                if (SimplexCore.AUTO_RESOLVE) {
                    string msg = "";
                    for (int i = 0; i < 100; i++) {
                        try {
                            SimplexCore.get().nextStep();
                        } catch (Exception ex) {
                            msg = ex.Message;
                        }
                    }
                } else {
                    SimplexCore.get().nextStep();
                }

                renderBasis();
                renderMatrix();
                renderFunction();
            } catch (Exception ex) {
                Console.Out.WriteLine(ex.ToString());
                Console.Out.WriteLine(ex.StackTrace);
                statusBar.Content = ex.Message;
            }
        }

        private void ButtonPrev(object sender, RoutedEventArgs e) {
            try {
                SimplexCore.get().prevStep();
                renderBasis();
                renderMatrix();
                renderFunction();
            } catch (Exception ex) {
                statusBar.Content = ex.Message;
            }
        }

        private void ButtonSolidSetClick(object sender, RoutedEventArgs e) {
            Button clicked = (Button) sender;
            string name = clicked.Name;
            string[] rawX = name
                .Replace("_", "")
                .Replace("and", "#")
                .Split('#');

            int xa = Utils.readInt32(rawX[0], statusBar);
            int xb = Utils.readInt32(rawX[1], statusBar);

            if (SimplexCore.get().status == SimplexStatus.SIMPLEX) {
                SimplexCore.get().simplexTable.xa = xa;
                SimplexCore.get().simplexTable.xb = xb;
            } else if (SimplexCore.get().status == SimplexStatus.HELPER) {
                SimplexCore.get().helper.simplexTable.xa = xa;
                SimplexCore.get().helper.simplexTable.xb = xb;
            }


            renderMatrix();
        }

        private void ButtonWithBasis(object sender, RoutedEventArgs e) {
            renderBasis();
        }

        private void ButtonLoadFile(object sender, RoutedEventArgs e) {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true) {
                string filename = dlg.FileName;
                string fileText = File.ReadAllText(filename);

                graphicWindow?.Close();

                int row, column;
                SimplexFunction function;
                Fraction[] basis;
                Fraction[][] matrix;

                var lines = fileText.Split('\n');
                var lineWithRowAndColumn = lines[0].Split(' ');
                row = Int32.Parse(lineWithRowAndColumn[0]);
                column = Int32.Parse(lineWithRowAndColumn[1]);

                var lineWithFunction = lines[1].Split(' ');
                Fraction[] funcArgs = new Fraction[column];
                for (int i = 0; i < funcArgs.Length; i++) {
                    funcArgs[i] = new Fraction(lineWithFunction[i]);
                }

                SimplexFunction.FunctionType type = lineWithFunction[column].Equals("max")
                    ? SimplexFunction.FunctionType.MAX
                    : SimplexFunction.FunctionType.MIN;

                function = new SimplexFunction(funcArgs, type);

                var lineWithBasis = lines[2].Split(' ');
                basis = new Fraction[column - 1];
                for (int i = 0; i < lineWithBasis.Length; i++) {
                    if (lineWithBasis[i].Equals("")) {
                        continue;
                    }

                    basis[i] = new Fraction(lineWithBasis[i]);
                }

                matrix = new Fraction[row][];
                for (int r = 0; r < row; r++) {
                    Fraction[] rowMatrix = new Fraction[column];
                    var lineWithLimitation = lines[3 + r].Split(' ');
                    for (int c = 0; c < rowMatrix.Length; c++) {
                        rowMatrix[c] = new Fraction(lineWithLimitation[c]);
                    }

                    matrix[r] = rowMatrix;
                }

                SimplexCore.drop();
                SimplexCore.get().rowCount = row;
                SimplexCore.get().columnCount = column;
                SimplexCore.get().function = function;
                SimplexCore.get().basis = basis;
                SimplexCore.get().matrix = matrix;

                rowBox.Text = row + "";
                columnBox.Text = column + "";
                statusBar.Content = "";

                renderBasis();
                renderMatrix();
                renderFunction();
            }
        }

        private void ButtonSaveFile(object sender, RoutedEventArgs e) {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true) {
                if (SimplexCore.get().status != SimplexStatus.INPUT) {
                    throw new Exception("Сохранить в файл можно только на этапе ввода");
                }

                SimplexCore.get().readData();

                string filename = dlg.FileName;

                // Сохранение количества строк и колонок
                String text = "";
                text += string.Format("{0} {1}", SimplexCore.get().rowCount, SimplexCore.get().columnCount) + "\n";

                // Сохранение аргументов функции
                for (int i = 0; i < SimplexCore.get().function.argsMin.Length; i++) {
                    text += SimplexCore.get().function.args[i] + " ";
                }

                text += "0 ";

                text += SimplexCore.get().function.type.ToString().ToLower() + "\n";

                // Сохранение базиса
                for (int i = 0; i < SimplexCore.get().basis.Length; i++) {
                    text +=
                        (SimplexCore.get().basis[i] == null ? Fraction.ZERO : SimplexCore.get().basis[i]).ToString() +
                        " ";
                }

                text += "\n";

                // Сохранение матрицы
                Fraction[][] matrix = SimplexCore.get().matrix;
                for (int r = 0; r < matrix.Length; r++) {
                    for (int c = 0; c < matrix[r].Length; c++) {
                        text += matrix[r][c] + " ";
                    }

                    text += "\n";
                }

                File.WriteAllText(filename, text);
            }
        }

        private void TenFractionBoxClick(object sender, RoutedEventArgs e) {
            DefFractionBox.IsChecked = false;
            Fraction.DEFAULT_TYPE = false;
            renderBasis();
            renderMatrix();
            renderFunction();
        }

        private void DefFractionBoxClick(object sender, RoutedEventArgs e) {
            TenFractionBox.IsChecked = false;
            Fraction.DEFAULT_TYPE = true;
            renderBasis();
            renderMatrix();
            renderFunction();
        }

        private void ButtonAutoModeClick(object sender, RoutedEventArgs e) {
            StepsModeBox.IsChecked = false;
            SimplexCore.AUTO_RESOLVE = true;
        }

        private void ButtonStepsModeClick(object sender, RoutedEventArgs e) {
            AutoModeBox.IsChecked = false;
            SimplexCore.AUTO_RESOLVE = false;
        }

        private void ButtonHelp(object sender, RoutedEventArgs e) {
            HelpWindow helpWindow = new HelpWindow();
            helpWindow.Show();
        }

        private void ButtonGraphic(object sender, RoutedEventArgs e) {
            new GraphicWindow(null);
        }
    }
}
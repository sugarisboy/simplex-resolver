﻿using System;

namespace Simplex {
    public class Test {

        public static void test() {

            Fraction[][] matrix = new Fraction[3][];
            //matrix[0] = new Fraction[] {new Fraction("2"), new Fraction("1"), new Fraction("1"), new Fraction("1"), new Fraction("2")};
            //matrix[1] = new Fraction[] {new Fraction("1"), new Fraction("1"), new Fraction("-1"), new Fraction("0"), new Fraction("-2")};
            //matrix[2] = new Fraction[] {new Fraction("3"), new Fraction("1"), new Fraction("-1"), new Fraction("2"), new Fraction("2")};
            //
            //var vector = new Fraction[] {new Fraction("1"), new Fraction("0"), new Fraction("1"), new Fraction("1")};

            matrix[0] = new Fraction[] {new Fraction("-1"), new Fraction("0"), new Fraction("1"), new Fraction("-2"), new Fraction("-2"), new Fraction("-2")};
            matrix[1] = new Fraction[] {new Fraction("0"), new Fraction("1"), new Fraction("-1"), new Fraction("1"), new Fraction("-2"), new Fraction("0")};
            matrix[2] = new Fraction[] {new Fraction("2"), new Fraction("1"), new Fraction("0"), new Fraction("5"), new Fraction("1"), new Fraction("7")};

            var vector = new Fraction[] {new Fraction("1"), new Fraction("0"), new Fraction("0"), new Fraction("1"), new Fraction("1"), new Fraction("0")};
            

            Gause gause = new Gause(matrix, vector);
            Console.Out.WriteLine("Создано новое состояние: \n   -" + gause.message + ": \n" + Utils.castMatrixToString(gause.matrix));
            
            int step = 1;
            var nextStep = gause.calculate();

            while (nextStep != null) {
                Console.Out.WriteLine("Создано новое состояние: \n   -" + nextStep.message + ": \n" + Utils.castMatrixToString(nextStep.matrix));
                nextStep = nextStep.prevState;
                step++;
            }
        }

        public static void test2() {
            Fraction[][] matrix = new Fraction[2][];
            matrix[0] = new Fraction[] {new Fraction("1"), new Fraction("1"), new Fraction("-2"), new Fraction("3"), new Fraction("1")};
            matrix[1] = new Fraction[] {new Fraction("2"), new Fraction("-1"), new Fraction("-1"), new Fraction("3"), new Fraction("2")};

            var vector = new Fraction[] {new Fraction("0"), new Fraction("0"), new Fraction("1"), new Fraction("1")};
            Gause gause = new Gause(matrix, vector).calculate();

            Fraction[] func = new Fraction[] {new Fraction("-1"), new Fraction("-2"), new Fraction("1"), new Fraction("-1")};
            SimplexFunction function = new SimplexFunction(func, SimplexFunction.FunctionType.MIN);
            
            function.calculateFromGause(gause);
        }

        public static void test3() {
            Fraction[][] matrix = new Fraction[2][];
            matrix[0] = new Fraction[] {new Fraction("1"), new Fraction("4"), new Fraction("1"), new Fraction("3")};
            matrix[1] = new Fraction[] {new Fraction("1"), new Fraction("-2"), new Fraction("-1"), new Fraction("-1")};

            var vector = new Fraction[] {new Fraction("1"), new Fraction("1"), new Fraction("0")};
            Gause gause = new Gause(matrix, vector).calculate();

            Fraction[] func = new Fraction[] {new Fraction("-1"), new Fraction("2"), new Fraction("-1"), new Fraction("0")};
            SimplexFunction function = new SimplexFunction(func, SimplexFunction.FunctionType.MIN);

            var calculateFromGause = function.calculateFromGause(gause);

            new SimplexTable(gause, function);
        }
    }
}
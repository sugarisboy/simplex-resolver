﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Simplex {
    class SimplexCore {
        public static bool AUTO_RESOLVE = false;

        public Fraction[][] matrix;
        public Fraction[] basis;
        public SimplexFunction function;

        public SimplexStatus status = SimplexStatus.INPUT;

        public int rowCount = 3;
        public int columnCount = 4;

        // Gause
        public int gauseStep = 0;
        public LinkedList<Gause> gauseStates = new LinkedList<Gause>();

        // Simplex
        public SimplexTable simplexTable;

        // Helper
        public SimplexTableHelper helper;

        private static SimplexCore instance;

        public static SimplexCore get() {
            if (instance == null) {
                drop();
            }

            return instance;
        }

        public static void drop() {
            instance = new SimplexCore();
            instance.initialize();
        }

        private void initialize() {

            // init matrix
            matrix = new Fraction[rowCount][];
            for (int r = 0; r < rowCount; r++) {
                Fraction[] row = new Fraction[columnCount];
                for (int c = 0; c < columnCount; c++) {
                    row[c] = new Fraction("0");
                }

                matrix[r] = row;
            }

            // init basis
            basis = new Fraction[columnCount - 1];
            for (int c = 0; c < columnCount - 1; c++) {
                basis[c] = new Fraction("0");
            }

            // init function
            function = new SimplexFunction(basis, SimplexFunction.FunctionType.MIN);
        }

        public void setStatusBar(string msg) {
            MainWindow.get().statusBar.Content = msg;
        }

        public void readData() {
            matrix = UiUtils.readMatrix(MainWindow.get().RightBar, rowCount, columnCount);
            basis = MainWindow.get().BasisPanel.Children.Count != 0
                ? UiUtils.readRow((StackPanel) MainWindow.get().BasisPanel.Children[0], columnCount - 1)
                : new Fraction[columnCount - 1];
            Fraction[] functionArgs = UiUtils.readRow((StackPanel) MainWindow.get().FunctionPanel.Children[0],
                columnCount - 1);
            ComboBox functionComboBox = (ComboBox) MainWindow.get().FunctionPanel.Children[1];
            SimplexFunction.FunctionType type = functionComboBox.Text.Equals("max")
                ? SimplexFunction.FunctionType.MAX
                : SimplexFunction.FunctionType.MIN;
            function = new SimplexFunction(functionArgs, type);
        }

        public MatrixWithDescription getMatrix() {
            if (status == SimplexStatus.GAUSE && gauseStates.Count != 0) {
                Gause step = gauseStates.ElementAt(gauseStep);
                return new MatrixWithDescription(step.matrix, step.message, status);
            } else if (status == SimplexStatus.HELPER) {
                return new MatrixWithDescription(helper.simplexTable.matrix, helper.simplexTable.message, status);
            } else if (status == SimplexStatus.SIMPLEX) {
                return new MatrixWithDescription(simplexTable.matrix, simplexTable.message, status);
            } else if (status == SimplexStatus.RESULT) {
                return new MatrixWithDescription(simplexTable.matrix, "Ответ:", status);
            }

            return new MatrixWithDescription(matrix, "Исходная матрица", status);
        }

        public void nextStep() {
            if (status == SimplexStatus.INPUT) {
                readData();

                // Приведение к каноническому виду
                for (int r = 0; r < matrix.Length; r++) {
                    if (matrix[r][matrix[0].Length - 1].toDouble() >= 0)
                        continue;
                    for (int c = 0; c < matrix[0].Length; c++) {
                        matrix[r][c] = matrix[r][c].multiple(new Fraction(-1, 1));
                    }
                }
                
                if (MainWindow.get().CheckBoxBasis.IsChecked == true) {
                    status = SimplexStatus.GAUSE;
                    calculateGause();
                } else {
                    status = SimplexStatus.HELPER;
                    helper = new SimplexTableHelper(matrix);
                }
            } else if (status == SimplexStatus.GAUSE) {
                if (gauseStep == gauseStates.Count - 1) {
                    status = SimplexStatus.SIMPLEX;
                    simplexTable = new SimplexTable(gauseStates.ElementAt(gauseStep), function);
                    new GraphicWindow(simplexTable);
                } else {
                    gauseStep++;
                }
            } else if (status == SimplexStatus.HELPER) {
                if (AUTO_RESOLVE) {
                    helper.simplexTable.findAutomaticSolid();
                }
                
                if (!helper.simplexTable.hasSolve) {
                    setStatusBar("Задача не имеет решения");
                } else if (helper.simplexTable.finished) {
                    setStatusBar("Вспомогательная задача решена");
                    status = SimplexStatus.SIMPLEX;
                    simplexTable = new SimplexTable(helper, function);
                    new GraphicWindow(simplexTable);
                } else if (helper.simplexTable.hasSolve) {
                    if (helper.simplexTable.finished) {
                        setStatusBar("Вспомогательная задача решена");
                        status = SimplexStatus.SIMPLEX;
                        simplexTable = new SimplexTable(helper, function);
                    } else {
                        helper = helper.swap();
                    }
                } else {
                    setStatusBar("Неопознанный статус");
                }
            } else if (status == SimplexStatus.SIMPLEX) {
                if (AUTO_RESOLVE) {
                    simplexTable.findAutomaticSolid();
                }
                
                if (!simplexTable.hasSolve) {
                    setStatusBar("Задача не имеет решения");
                } else if (simplexTable.finished) {
                    setStatusBar("Задача решена");
                    status = SimplexStatus.RESULT;
                } else if (simplexTable.hasSolve) {
                    if (simplexTable.finished) {
                        setStatusBar("Задача решена");
                    } else {
                        simplexTable = simplexTable.swap();
                    }
                } else {
                    setStatusBar("Неопознанный статус");
                }
            }
        }

        public void prevStep() {
            if (status == SimplexStatus.INPUT) {
                throw new Exception("Вы уже на первом шаге");
            } else if (status == SimplexStatus.GAUSE) {
                if (gauseStep == 0) {
                    status = SimplexStatus.INPUT;
                } else {
                    gauseStep--;
                }
            } else if (status == SimplexStatus.HELPER) {
                if (helper.prevState == null) {
                    status = SimplexStatus.INPUT;
                } else {
                    helper = helper.prevState;
                }
            } else if (status == SimplexStatus.SIMPLEX) {
                if (simplexTable.prevState != null) {
                    simplexTable = simplexTable.prevState;
                } else {
                    if (MainWindow.get().CheckBoxBasis.IsChecked == true) {
                        status = SimplexStatus.GAUSE;
                    } else {
                        status = SimplexStatus.HELPER;
                    }
                }
            } else if (status == SimplexStatus.RESULT) {
                status = SimplexStatus.SIMPLEX;
            }
        }

        public void calculateGause() {
            Gause gause = new Gause(matrix, basis).calculate();
            LinkedList<Gause> states = new LinkedList<Gause>();

            Gause nextState = gause;
            while (nextState != null) {
                states.AddFirst(nextState);
                nextState = nextState.prevState;
            }

            this.gauseStates = states;
            this.gauseStep = 0;
        }

        public void updateRowAndColumn(int rowCount, int columnCount) {
            this.rowCount = rowCount;
            this.columnCount = columnCount;
            this.initialize();
        }
    }
}
﻿using System;

namespace Simplex {
    public class Fraction {
        public static Fraction ZERO = new Fraction(0, 1);
        public static Fraction ONE = new Fraction(1, 1);
        public static Fraction MINUS_ONE = new Fraction(-1, 1);
        
        public static bool DEFAULT_TYPE = true;
        public int num { get; }
        public int denom { get; }

        public Fraction(String s) {
            if (s.Trim().Equals("")) {
                num = 0;
                denom = 1;
            } else {
                int n, d;
                if (s.Contains(".") || s.Contains(",")) {
                    double number = Double.Parse(s);
                    d = 1_000_000;
                    n = (int) (number * d);
                } else {
                    String[] split = s.Split('/');
                    if (split.Length == 1) {
                        n = Int32.Parse(split[0]);
                        d = 1;
                    } else {
                        n = Int32.Parse(split[0]);
                        d = Int32.Parse(split[1]);
                    }
                }

                if (d == 0) {
                    throw new Exception("Знаменатель не может быть равен 0");
                }
            
                int min = Math.Min(Math.Abs(n), Math.Abs(d));
                for (int q = 2; q < min + 1; q++) {
                    if (n % q == 0 && d % q == 0) {
                        n /= q;
                        d /= q;
                        q = 1;
                    }
                }

                if (d < 0) {
                    n *= -1;
                    d *= -1;
                }

                if (n == 0) {
                    d = 1;
                }

                num = n;
                denom = d;
            }
        }

        public Fraction(int n, int d) {
            if (d == 0) {
                throw new Exception("Знаменатель не может быть равен 0");
            }
            
            int min = Math.Min(Math.Abs(n), Math.Abs(d));
            for (int q = 2; q < min + 1; q++) {
                if (n % q == 0 && d % q == 0) {
                    n /= q;
                    d /= q;
                    q = 1;
                }
            }

            if (d < 0) {
                n *= -1;
                d *= -1;
            }

            if (n == 0) {
                d = 1;
            }
            
            num = n;
            denom = d;
        }

        public Fraction sum(Fraction f2) {
            int dtemp = denom * f2.denom;
            int ntemp = this.num * f2.denom + this.denom * f2.num;
            return new Fraction(ntemp, dtemp);
        }

        public Fraction minus(Fraction f2) {
            int dtemp = this.denom * f2.denom;
            int ntemp = this.num * f2.denom - this.denom * f2.num;
            return new Fraction(ntemp, dtemp);
        }

        public Fraction multiple(Fraction f2) {
            return new Fraction(this.num * f2.num, this.denom * f2.denom);
        }

        public Fraction divide(Fraction f2) {
            if (f2.num == 0) {
                throw new DivideByZeroException();
            }

            return new Fraction(this.num * f2.denom, this.denom * f2.num);
        }

        public Fraction flip() {
            return new Fraction(this.denom, this.num);
        }

        public double toDouble() {
            return num / (double) denom;
        }
        
        public Fraction clone() {
            return new Fraction(this.num, this.denom);
        }

        public override bool Equals(object obj) {
            if (obj is int) {
                if (obj.Equals(0)) {
                    return num == 0 || denom == 0;
                } else {
                    return Math.Abs(denom) == 1 && obj.Equals(num);
                }
            }

            if (obj is Fraction) {
                if (obj == ZERO) {
                    return num == 0 || denom == 0;
                }

                return this.toDouble().Equals(((Fraction) obj).toDouble());
            }

            return base.Equals(obj);
        }

        public override string ToString() {
            if (DEFAULT_TYPE) {
                return denom == 1 ? num.ToString() : String.Format("{0}/{1}", num, denom);
            } else {
                return String.Format("{0:0.###}", toDouble());
            }
        }
    }
}
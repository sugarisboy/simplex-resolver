﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplex {
    public class Gause {
        public Fraction[][] matrix { get; set; }
        public Fraction[] basis;
        public Gause prevState = null;
        public Gause nextState = null;
        
        // Прямой ход
        public bool nextMove;

        // Рассматриваемая колонка
        public int currentColumn;

        public string message;

        public bool isNormalized;
        
        // Вызов метода гауса
        public Gause(Fraction[][] matrix, Fraction[] basis) {
            this.matrix = matrix;
            this.basis = basis;
            this.nextMove = true;
            this.currentColumn = 0;
            this.message = "Исходная матрица для метода Гауса";
            this.isNormalized = false;
        }

        private Gause(Fraction[][] matrix, Fraction[] basis, Gause prevState) : this(matrix, basis) {
            this.prevState = prevState;
        }

        private Gause normilize() {
            int rowCount = matrix.Length;
            int columnCount = matrix[0].Length;
            Fraction[][] normalized = new Fraction[rowCount][];
            for (int row = 0; row < rowCount; row++) {
                Fraction[] column = new Fraction[columnCount];
                int offset = 0;
                for (int c = 0; c < columnCount - 1; c++) {
                    if (basis[c].Equals(0)) {
                        offset++;
                    } else {
                        column[c - offset] = matrix[row][c];
                    }
                }

                int startIndex = columnCount - offset - 1;
                offset = 0;
                for (int c = 0; c < columnCount - 1; c++) {
                    if (basis[c].Equals(0)) {
                        column[startIndex + offset] = matrix[row][c];
                        offset++;
                    }
                }

                column[columnCount - 1] = matrix[row][columnCount - 1];
                normalized[row] = column;
            }

            Gause gause = createNewState(normalized, basis, "Нормализация");
            gause.isNormalized = true;
            return gause;
        }

        public Gause calculate() {
            if (!isNormalized) {
                return normilize().calculate();
            }

            // Check have resolve
            int countBasisX = 0;
            for (int i = 0; i < matrix[0].Length - 1; i++) {
                if (!basis[i].Equals(Fraction.ZERO))
                    countBasisX++;
            }

            if (countBasisX != matrix.Length) {
                throw new Exception("Система не имеет решений, так как ранг матрицы меньше количества переменных");
            }

            Fraction[][] sourceMatrix = Utils.clone(matrix);
            Fraction[][] nextMatrix = Utils.clone(matrix);
            
            int rowLength = matrix.Length;
            int columnLength = matrix[0].Length;

            Gause current = this;
            
            //Прямой ход (Зануление нижнего левого угла)
            for (int k = 0; k < rowLength; k++) //k-номер строки
            {
                if (matrix[k][k].Equals(0)) {
                    continue;
                }
                
                if (matrix[k][k].Equals(Fraction.ZERO)) { // меняем строки местами в случае если элемнт равен 0
                    for (int rowAfterK = 0; rowAfterK < rowLength; rowAfterK++) {
                        if (!matrix[rowAfterK][k].Equals(Fraction.ZERO)) {
                            nextMatrix = Utils.cloneWithSwapRow(nextMatrix, k, rowAfterK);
                            string message = String.Format("Смена строк {0} и {1}", k, rowAfterK);
                            current = createNewState(nextMatrix, basis, message, current).calculate();
                            this.matrix = Utils.clone(nextMatrix);
                        }
                    }
                }

                for (int i = 0; i < columnLength; i++) //i-номер столбца
                    nextMatrix[k][i] = nextMatrix[k][i].divide(matrix[k][k]); //Деление k-строки на первый член !=0 
                for (int i = k + 1; i < rowLength; i++) //i-номер следующей строки после k
                {
                    Fraction K = nextMatrix[i][k].divide(nextMatrix[k][k]); //Коэффициент
                    for (int j = 0; j < columnLength; j++) //j-номер столбца следующей строки после k
                        //Зануление элементов матрицы ниже первого члена, преобразованного в единицу
                        nextMatrix[i][j] = nextMatrix[i][j].minus(nextMatrix[k][j].multiple(K));
                }

                current = createNewState(nextMatrix, basis, "Прямой ход для строки K = " + k, current);
                this.matrix = Utils.clone(nextMatrix);
            }

            //Обратный ход (Зануление верхнего правого угла)
            for (int k = rowLength - 1; k > -1; k--) //k-номер строки
            {
                if (matrix[k][k].Equals(0)) {
                    continue;
                }
                
                for (int i = columnLength - 1; i > -1; i--) //i-номер столбца
                    nextMatrix[k][i] = nextMatrix[k][i].divide(matrix[k][k]);
                for (int i = k - 1; i > -1; i--) //i-номер следующей строки после k
                {
                    Fraction K = nextMatrix[i][k].divide(nextMatrix[k][k]);
                    for (int j = columnLength - 1; j > -1; j--) //j-номер столбца следующей строки после k
                        nextMatrix[i][j] = nextMatrix[i][j].minus(nextMatrix[k][j].multiple(K));
                }

                for (int r = 0; r < matrix.Length; r++) {
                    int notZero = 0;
                    int index = -1;
                    for (int c = 0; c < matrix[r].Length - 1; c++) {
                        if (!matrix[r][c].Equals(0)) {
                            notZero++;
                            index = c;
                        }
                    }

                    if (notZero == 1) {
                        for (int c = 0; c < matrix[r].Length; c++) {
                            nextMatrix[r][c] = nextMatrix[r][c].multiple(nextMatrix[r][index].flip());
                        }
                    }
                }
                
                current = createNewState(nextMatrix, basis, "Обратный ход для строки K = " + k, current);
            }

            this.matrix = sourceMatrix;
            return createNewState(nextMatrix, basis, "Решение методом Гауса", current);
        }

        public Gause createNewState(Fraction[][] matrix, Fraction[] basis, string message = "", Gause prevState = null) {
            Gause gause = new Gause(Utils.clone(matrix), basis, prevState ?? this);
            gause.message = message;
            this.nextState = gause;
            return gause;
        }

        public Fraction[][] getMatrix() {
            return null;
        }
    }
}